HAI 1.2
CAN HAS STDIO?

OBTW
This script cycles through a series of responses to user input, ignoring the input's content.
Each cycle increases a counter and selects a response based on the counter's value.
After displaying the tenth response, it exits.

This is designed to be a more educational script for my own leaning purposes (and to help burnout), 
so as such, python equivalent snippets will get commented in, starting with a "py"
TLDR

I HAS A COUNTER ITZ 0
BTW py COUNTER = 0

VISIBLE "O HAI LOL, THIS IZ UR TRUSTY CATBOT 1.0 BETA. PLZ TYPE ANYTHING AND PRESS ENTER UWU"

IM IN YR LOOP UPPIN YR COUNTER TIL BOTH SAEM COUNTER AN 10
  I HAS A USERINPUT
  GIMMEH USERINPUT
  
  OBTW py COUNTER = 0  # Assuming COUNTER is initialized before the loop starts
while COUNTER < 10:
    USERINPUT = input("Enter something: ")
    COUNTER += 1
    TLDR

  BOTH SAEM COUNTER AN 1, O RLY?
    YA RLY
      VISIBLE "RESPONSE 1: O HAI!"
  OIC

  BOTH SAEM COUNTER AN 2, O RLY?
    YA RLY
      VISIBLE "RESPONSE 2: HOWZ U DOIN?"
  OIC

  BOTH SAEM COUNTER AN 3, O RLY?
    YA RLY
      VISIBLE "RESPONSE 3: MEOW MEOW MEOW"
  OIC

  BOTH SAEM COUNTER AN 4, O RLY?
    YA RLY
      VISIBLE "RESPONSE 4: DURR!"
  OIC

  BOTH SAEM COUNTER AN 5, O RLY?
    YA RLY
      VISIBLE "RESPONSE 5: GOT ANY SNACKS?"
  OIC

  BOTH SAEM COUNTER AN 6, O RLY?
    YA RLY
      VISIBLE "RESPONSE 6: I CAN HAZ CHEEZBURGER?"
  OIC

  BOTH SAEM COUNTER AN 7, O RLY?
    YA RLY
      VISIBLE "RESPONSE 7: LOL, DAT'S FUNNY!"
  OIC

  BOTH SAEM COUNTER AN 8, O RLY?
    YA RLY
      VISIBLE "RESPONSE 8: TIME FOR A NAP, TTYL!"
  OIC

  BOTH SAEM COUNTER AN 10, O RLY?
    YA RLY
      VISIBLE "RESPONSE 10: TIME FOR A NAP, TTYL!"
  OIC
IM OUTTA YR LOOP

KTHXBYE
